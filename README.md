# sdeals api

- [sdealsserver.herokuapp.com](https://sdealsserver.herokuapp.com/)

## heroku
- [manage app](https://dashboard.heroku.com/apps/sdealsserver)

# commands

- ``heroku local web`` - Run the app locally

# node-js-getting-started

A barebones Node.js app using [Express 4](http://expressjs.com/).

This application supports the [Getting Started on Heroku with Node.js](https://devcenter.heroku.com/articles/getting-started-with-nodejs) article - check it out.

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) and the [Heroku CLI](https://cli.heroku.com/) installed.

```sh
$ git clone https://github.com/heroku/node-js-getting-started.git # or clone your own fork
$ cd node-js-getting-started
$ npm install
$ npm start
```

Your app should now be running on [localhost:5000](http://localhost:5000/).

## Deploying to Heroku

```
$ heroku create
$ git push heroku main
$ heroku open
```
or

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

## Documentation

For more information about using Node.js on Heroku, see these Dev Center articles:

- [Getting Started on Heroku with Node.js](https://devcenter.heroku.com/articles/getting-started-with-nodejs)
- [Heroku Node.js Support](https://devcenter.heroku.com/articles/nodejs-support)
- [Node.js on Heroku](https://devcenter.heroku.com/categories/nodejs)
- [Best Practices for Node.js Development](https://devcenter.heroku.com/articles/node-best-practices)
- [Using WebSockets on Heroku with Node.js](https://devcenter.heroku.com/articles/node-websockets)

## Node.js

- [Import and Export in Node.js](https://www.geeksforgeeks.org/import-and-export-in-node-js/)
- [How to encode or decode a URL in Node.js](https://attacomsian.com/blog/nodejs-encode-decode-url)

## Rebrandly - Branded Link Management

- [https://developers.rebrandly.com/docs](https://developers.rebrandly.com/docs)
- [rebrandly.js](https://github.com/ifvictr/rebrandly.js/)


## Good practise

- [Node.js project architecture best practices](https://blog.logrocket.com/the-perfect-architecture-flow-for-your-next-node-js-project/)
- [Separating logic from Express routes for easier testing](https://www.coreycleary.me/separating-logic-from-express-routes-for-easier-testing)

## Articles

- [Deploy Node Express App on Heroku: 8 Easy Steps](https://dzone.com/articles/deploy-your-node-express-app-on-heroku-in-8-easy-s)
- [How to improve node js code for production and split it properly across files?](https://stackoverflow.com/questions/52457807/how-to-improve-node-js-code-for-production-and-split-it-properly-across-files)
- [Building RESTful Api With Node.js, Express.Js And PostgreSQL the Right way](https://itnext.io/building-restful-api-with-node-js-express-js-and-postgresql-the-right-way-b2e718ad1c66)
