require('dotenv').config() // support .env file locally
const Rebrandly = require("rebrandly");

const REBRANDLY_API_KEY = process.env.REBRANDLY_API_KEY;
const rebrandlyClient = new Rebrandly({
    apikey: REBRANDLY_API_KEY,
});

let requestHeaders = {
    "Content-Type": "application/json",
    "apikey": REBRANDLY_API_KEY,
    // "workspace": "YOUR_WORKSPACE_ID"
}

module.exports = { rebrandlyClient, requestHeaders }
