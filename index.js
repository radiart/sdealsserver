require('dotenv').config() // support .env file locally
const express = require('express')
const path = require('path')
// const {pool} = require('./config/db');
const {createLink} = require('./controllers/rebrandly/createLink')
const {deleteLink}  = require('./controllers/rebrandly/deleteLink')
const {deleteOldLinks}  = require('./controllers/rebrandly/deleteOldLinks')

const PORT = process.env.PORT || 5000

express()
	.use(express.static(path.join(__dirname, 'public')))
	.set('views', path.join(__dirname, 'views'))
	.set('view engine', 'ejs')
	.get('/', (req, res) => res.render('pages/index'))
	// .get('/config', async (req, res) => {
	// 	try {
	// 		const client = await pool.connect();
	// 		const result = await client.query('SELECT * FROM sales');
	// 		const results = { 'results': (result) ? result.rows : null};
	// 		res.render('pages/config', results );
	// 		client.release();
	// 	} catch (err) {
	// 		console.error(err);
	// 		res.send("Error " + err);
	// 	}
	// })
	.get('/newlink', createLink)
	.get('/deleteLink', deleteLink)
	.get('/deleteOldLinks', deleteOldLinks)
	.listen(PORT, () => console.log(`Listening on ${ PORT }`))
