require('dotenv').config() // support .env file locally
const { rebrandlyClient, requestHeaders } = require('../../config/rebrandly');
const axios = require('axios');

// const runsLimit = 4;
const linksPerCall = 25;
const linksLimit = 25;

const dateFromXDaysBefore = (days) => {
    let date = new Date();
    return new Date(date.getTime() - (days * 24 * 60 * 60 * 1000));
}

const deleteLink = async (id) => {
    console.log('delete link id: ', id);
    try {
        const result = await rebrandlyClient.links.delete(id);
        const {status} = result;
        console.log('result status', status);
        if(status == 'deleted') return true;
        return false;
    } catch (err) {
        console.error(err);
        return "Error " + err
    }
}

const hasNumber = myString => {
    return /\d/.test(myString);
}

const isCustomLink = slashtag => {
    if(!hasNumber(slashtag) && slashtag.length > 4) return true;
    return false;
}

const deleteLinks = async (links) => {
    for(let i = 0; i < links.length; i++){
        links[i].deleted = false;
        const { id, slashtag } = links[i];
        const customLink = isCustomLink(slashtag)
        if(!customLink) {
            const linkDeleted = await deleteLink(id)
            if(linkDeleted) links[i].deleted = true;
        }
    }
    return links;
}

const isAisOlderThenB = (date1,date2) => {
    const d1 = new Date(date1);
    const d2 = new Date(date2);
    if(d1 < d2) return true;
    return false;
}

const filterLinksByDate = (data, untilDate) => {
    console.log('filterLinksByDate untilDate',untilDate)
    return data.filter((link)=>{
        const {createdAt} = link;
        console.log('filterLinksByDate createdAt',createdAt)
        return isAisOlderThenB(createdAt, untilDate)
    })
}

const getLinks = async (untilDate, oldResults = [], lastId = '', howManyRuns = 0) => {

        console.log('untilDate',untilDate)
        console.log('oldResults len',oldResults.length)
        console.log('lastId',lastId)
        console.log('howManyRuns',howManyRuns)

        let results = [...oldResults];

        const queryParams = {
            orderBy: 'createdAt',
            orderDir: 'asc',
            last: lastId
        }

        try {
            let url = 'https://api.rebrandly.com/v1/links?orderBy=createdAt&orderDir=asc'
            if(lastId) url = url + `&last=${lastId}`
            const {data} = await axios({
                method: 'get',
                url: url,
                data: JSON.stringify(queryParams),
                headers: requestHeaders
            });

            const filteredLinks = filterLinksByDate(data, untilDate);
            console.log('filteredLinks',filteredLinks.length)

            results = [...results,...filteredLinks];

            // if there is more deleteOldLinks call the function again
            if(filteredLinks.length === linksPerCall && data.length === linksPerCall && results.length <= linksLimit) {
                const lastId = data[linksPerCall - 1].id || '';
                return await getLinks(untilDate, results,lastId, howManyRuns + 1)
            } else {
                return results;
            }

        } catch (error) {
            console.log(error.response); // this is the main part. Use the response property from the error object
            return error.response;
        }
}

// const daysFromNow = () => {
//     var timestamp = new Date().getTime() + (30 * 24 * 60 * 60 * 1000)
// }

const deleteOldLinks = async (req, res) => {

    // Delete only link older then 3 months from now
    const threeMonthsBeforeToday = dateFromXDaysBefore(90);
    console.log('threeMonthsBeforeToday',threeMonthsBeforeToday)
    // const testDate = '2020-04-19T17:23:34.000Z';

    let results  = [];
    results = await getLinks(threeMonthsBeforeToday);

    console.log(results.length)

    const deletedLinks = await deleteLinks(results);

    res.render('pages/deleteOldLinks', {results: deletedLinks} );
}

module.exports = { deleteOldLinks }
