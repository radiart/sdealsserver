require('dotenv').config() // support .env file locally
const {rebrandlyClient} = require('../../config/rebrandly');
// const {pool} = require('../../config/db');
// const REBRANDLY_DOMAIN = process.env.REBRANDLY_DOMAIN;

const deleteLink = async (req, res) => {

    const id = req.query.id;
    console.log('link id to delete: ',id);

    if(id){
        try {
            const result = await rebrandlyClient.links.delete(id);
            console.log(result)
            res.send(result);
        } catch (err) {
            console.error(err);
            res.send("Error " + err);
        }
    } else {
        res.send('please provide link id');
    }
}

module.exports = { deleteLink }
