require('dotenv').config() // support .env file locally
const {rebrandlyClient} = require('../../config/rebrandly');
const {pool} = require('../../config/db');
const REBRANDLY_DOMAIN = process.env.REBRANDLY_DOMAIN;

const isLinkExist = async (linkToShorten) => {
    let linkExist = null;
    try {
        const client = await pool.connect();
        const query = `select * from links where url = '${linkToShorten}' limit 1`
        console.log('query',query)
        const result = await client.query(query);
        linkExist = (result) ? result.rows[0] : null;
    } catch (err) {
        console.error(err);
    }
    return linkExist
}

const createNewLink = async (linkToShorten) => {
    let newLink = null;
    try {
        const params = {
            destination: linkToShorten,
            domain: {
                id: REBRANDLY_DOMAIN,
                ref: `/domains/${REBRANDLY_DOMAIN}`,
            },
        }
        newLink = await rebrandlyClient.links.create(params)
        console.log('new link created',newLink.shortUrl)
    } catch (err) {
        console.error(err);
    }
    return newLink;
}

const saveShortenLink = async (newLink) => {
    let savedLink = null;
    const {destination = '', shortUrl = '', createdAt = ''} = newLink;
    try {
        const client = await pool.connect();
        const query = `INSERT INTO links (url, url_short, create_date) VALUES('${destination}', '${shortUrl}', '${createdAt}')`
        console.log('query',query)
        let result = await client.query(query);
        client.release();
        if(result) {
            console.log('link saved',savedLink)
            return newLink;
        }
    } catch (err) {
        console.error(err);
    }
    return savedLink;
}

const createLink = async (req, res) => {
    const {url, json} =  req.query
    const linkToShorten = decodeURI(url);
    console.log('link to shorten: ',linkToShorten)

    if(linkToShorten) {

        // Check if link exist in database
        let linkExist = await isLinkExist(linkToShorten)

        // Link doesnt exist
        if(!linkExist) {
            console.log('link doesnt exist')

            // Create shorten link in rebrandly
            let newLink = await createNewLink(linkToShorten);

            // Save the shorten link in database
            if(newLink) {
                const savedLink = await saveShortenLink(newLink)
                console.log('savedLink 2',savedLink)
                if(savedLink){
                    res.json({url: savedLink.shortUrl})
                 }
            }

        // Link exist
        } else {
            console.log('link exist',linkExist)
            res.json({url: linkExist.url_short})
        }
    } else {
        res.send('please provide the url to shorten')
    }
}

module.exports = { createLink }
